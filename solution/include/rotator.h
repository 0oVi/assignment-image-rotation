#ifndef ROTATOR_H
#define ROTATOR_H

#include "image.h"

/* Создает копию изображения, которая повернута на 90 градусов */
struct image* rotate(struct image const* source);
#endif

