#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>

#define bfTypeDefault 0x4d42
#define biBitCountDefault 24
#define Default 0
#define biPlanesDefault 1
#define biSizeDefault 40


// Описание для gcc и clang
struct __attribute__ ((packed)) bmp_header;

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_OUT_OF_MEMORY
    /* коды других ошибок  */
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer */
enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_OUT_OF_MEMORY_ERROR
    /* коды других ошибок */
};

enum write_status to_bmp(FILE* out, struct image const* img);

#endif

