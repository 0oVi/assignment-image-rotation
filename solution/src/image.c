#include "image.h"
#include <stdlib.h>

struct image* create_image(uint64_t sourceWidth, uint64_t sourceHeight) {
    struct image* result = malloc(sizeof(struct image));

    if(result == NULL) return NULL;

    result -> data = malloc(sourceWidth * sourceHeight * sizeof(struct pixel));

    // Если не смогли аллоцировать память под пиксели, смысла возвращать структуру нет
    if(result -> data == NULL) {
        free(result);
        return NULL;
    }

    result -> width = sourceWidth;
    result -> height = sourceHeight;

    return result;
}

void image_free(struct image* image) {
    if(image != NULL) free(image->data);
    free(image);
}

