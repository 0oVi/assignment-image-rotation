#include "rotator.h"
#include <stdio.h>

/* Создает копию изображения, которая повёрнута на 90 градусов */
struct image* rotate(struct image const* source) {
    uint64_t sourceWidth = source -> width;
    uint64_t sourceHeight = source -> height;

    struct image* result = create_image(sourceHeight, sourceWidth);

    if(result == NULL) return NULL;

    for (size_t i = 0; i < sourceHeight; i++) {
        for (size_t j = 0; j < sourceWidth; j++) {
            result -> data[(sourceHeight - i - 1) + j * sourceHeight] = source -> data[i * sourceWidth + j];
        }
    }

    return result;
}

