#include "bmp.h"
#include <stdlib.h>


struct __attribute__ ((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

const char empty[4] = { 0 };

static size_t calculate_padding(const struct image img) {
    return img.width * sizeof(struct pixel) % 4 == 0 ? 0 : 4 - img.width * sizeof(struct pixel) % 4;
}

struct bmp_header* create_default_bmp_header(uint64_t width, uint64_t height) {
    struct bmp_header* header = malloc(sizeof(struct bmp_header));
    if(header != NULL) {
        header->bfType = bfTypeDefault;
        header->bfileSize = sizeof(struct bmp_header) + (width * height * sizeof(struct pixel));
        header->bfReserved = Default;
        header->bOffBits = sizeof(struct bmp_header);
        header->biSize = biSizeDefault;
        header->biWidth = width;
        header->biHeight = height;
        header->biPlanes = biPlanesDefault;
        header->biBitCount = biBitCountDefault;
        header->biCompression = Default;
        header->biSizeImage = header -> bfileSize - header -> bOffBits;
        header->biXPelsPerMeter = Default;
        header->biYPelsPerMeter = Default;
        header->biClrUsed = Default;
        header->biClrImportant = Default;
        return header;
    }
    return NULL;
}

struct header_status {
    struct bmp_header header;
    enum read_status status;
};

struct header_status read_header(FILE* in) {
    enum read_status status = READ_OK;
    struct bmp_header bmpHeader;

    if (fread(&bmpHeader, sizeof(struct bmp_header), 1, in) != 1)
        status = READ_INVALID_SIGNATURE;

    if(bmpHeader.bfType != bfTypeDefault || bmpHeader.biBitCount != biBitCountDefault)
        status = READ_INVALID_HEADER;

    return (struct header_status) {
        .header = bmpHeader,
        .status = status
    };
}


enum read_status from_bmp(FILE *in, struct image* img) {

    struct header_status bmpHeaderStatus = read_header(in);
    if (bmpHeaderStatus.status != READ_OK) return bmpHeaderStatus.status;

    uint64_t width = img -> width = bmpHeaderStatus.header.biWidth;
    uint64_t height = img -> height = bmpHeaderStatus.header.biHeight;
    struct pixel* data = malloc(width * height * sizeof(struct pixel));

    if(data == NULL) {
        free(data);
        return READ_OUT_OF_MEMORY;
    }

    size_t padding = calculate_padding(*img);
    for(size_t i = 0; i < height; i++) {
        if(fread(data + i * width, width * sizeof(struct pixel), 1, in) != 1)
            return READ_INVALID_BITS;
        if(padding != 0)
            fseek(in, padding, SEEK_CUR);
    }

    img -> data = data;

    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    uint64_t width = img -> width;
    uint64_t height = img -> height;

    size_t padding = calculate_padding(*img);

    struct bmp_header* header = create_default_bmp_header(width, height);

    if(header == NULL) return WRITE_OUT_OF_MEMORY_ERROR;

    if(fwrite(header, sizeof(struct bmp_header), 1, out) != 1) {
        free(header);
        return WRITE_ERROR;
    }

    free(header);

    for(size_t i = 0; i < height; i++) {
        if(fwrite(img -> data + i * width, sizeof(struct pixel), width, out) != width) {
            return WRITE_ERROR;
        }
        if(padding != 0) {
            if(fwrite(&empty, sizeof(char), padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}

